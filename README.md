You can see a video of the project at
https://youtu.be/uwcnAvXwqqU

Files are availabe at
https://bitbucket.org/OOKAMII/coinops_cab_and_bartops_scripts_configuration/downloads/


INSTRUCTIONS

-------------------WINDOWS OPERATING SYSTEM INSTALLATION---------------------------------------

Install Windows 10, whatever version (Home, Pro, ....) on your cab or bartop
Install drivers for your device
Perform update with Windows Update

Install all Visual C++ Redistribuable runtime
https://www.techpowerup.com/download/visual-c-redistributable-runtime-package-all-in-one/

Install Direct X 9c with offline installer
https://www.microsoft.com/en-us/download/details.aspx?id=8109

Activate Net Framework 3.5 available in
CONTROL PANEL / ADD REMVOE SOFTARE / ADD OR REMOVE ADDITIONAL SOFTWARE

in windows settings, set hybernate and lock windows is at your own selection
if you want to lock or not system after inactivity

you can set automatic login by using netplwiz and deactivate user authentification requierment
you

to have no avatar picture you can download User-192.png and copy it to
C:\ProgramData\Microsoft\User Account Pictures\
and replace this file by the same by name

-----------------------------------------------------------------------------------------------------

-------------------SET UP ENVIRONMENT----------------------------------

- SET_USERNINT.reg
change registry value to be blank and avoid USERINIT.EXE to start windows explorer program (explorer.exe)

- SET_SHELL.reg
Change registry value to launch Coinops instead of default explorer.exe

For this step, SHELL start CoinOPS to
C:\CoinOPS\Cab scripts\Start_RetroFE.bat

If your CoinOPS is located to C drive and have the same folder name, you can use this reg
If not, you can define the right folder path by editing this registry file

- SET_UNIQUE_AVATAR_LOGIN_ICON.reg
This file is used to avoid avatar icon when login appear

- SET_DISABLE_LOGIN_BLUR_EFFECT.reg
This reg file is used to disable blur effect while login screen runing

- Start_RetroFE.bat
Batch file used to launch CoinOPS
This file is set in registry value SHELL in SET_SHELL.reg

--------------------------- RESTORE DESKTOP ENVIRONMENT--------------------------
- Launch_Desktop_run_as_administrator.bat
this bat file is to click right and run as administrator
because registry SHELL is set for Coinops executable, exiting frontend will show black screen

By pressing CTRL+ALT+SUPPR
Choose TASK MANAGER
Run EXPLORER.EXE as new task

You will be able to access default explorer and run this bat from hard drive or usb stick
This ill allow you also to run Windows Update and access settings
-------------------------------------------------------------------------------------


--------------------------- RESTORE CAB PROGRAM ENVIRONMENT--------------------------
- RestartWin_run_as_administrator.bat
after updating your OS, runing this bat file with right click will allow you to 
restart Windows and runing Coinops by default instead of default Windows desktop environment